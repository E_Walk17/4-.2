default : 4laba_2.o
	gcc -std=c99 -o exe 4laba_2.o

4laba_2.o : 4laba_2.c
	gcc -std=c99 -c 4laba_2.c

clean :
	rm *.o exe
